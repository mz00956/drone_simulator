
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Editor;

#if UNITY_EDITOR
[InitializeOnLoad]
#endif
public class OffsetProcessor : InputProcessor<float>
{
    #if UNITY_EDITOR
    static OffsetProcessor()
    {
        Initialize();
    }
    #endif

    [RuntimeInitializeOnLoadMethod]
    static void Initialize() 
    { 
        InputSystem.RegisterProcessor<OffsetProcessor>();
    }

    


    [Tooltip("The offset to add to the input value.")]
    public float offset = 0.0f;
    public override float Process(float value, InputControl control)
    {
        return value + offset;
    }
}

#if UNITY_EDITOR
public class OffsetProcessorEditor : InputParameterEditor<OffsetProcessor>
{
    private GUIContent m_OffsetLabel = new GUIContent("Offset by");
    protected override void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnGUI()
    {
        target.offset = EditorGUILayout.FloatField(m_OffsetLabel, target.offset);
    }
}
#endif