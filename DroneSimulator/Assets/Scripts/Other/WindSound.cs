using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindSound : MonoBehaviour
{
    [SerializeField]
    AudioSource audioSourceSpeed;
    [SerializeField]
    AudioSource audioSourceAltitude;
    Rigidbody rigidbody;

    void OnEnable()
    {
        rigidbody = GetComponentInParent<Rigidbody>();
    }

    void FixedUpdate()
    {
        audioSourceSpeed.volume = rigidbody.velocity.magnitude / 10;
        audioSourceAltitude.volume = Mathf.Clamp01((transform.position.y - 10) / 10);
    }
}
