using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceVelocity : MonoBehaviour
{
    Rigidbody rigidbody;

    void OnEnable()
    {
        rigidbody = GetComponentInParent<Rigidbody>();
    }


    void FixedUpdate()
    {
        Quaternion targetRotation = Quaternion.LookRotation(rigidbody.velocity, Vector3.up);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.fixedDeltaTime * 2);
    }
}
