using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicQueue : MonoBehaviour
{
    [SerializeField]
    private List<AudioClip> musicClips = new List<AudioClip>();
    private AudioSource audioSource;

    private int currentClipIndex = 0;

    void OnEnable()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = musicClips[currentClipIndex];
        audioSource.Play();
    }

    void Update()
    {
        if (!audioSource.isPlaying)
        {
            currentClipIndex = (currentClipIndex + 1) % musicClips.Count;
            audioSource.clip = musicClips[currentClipIndex];
            audioSource.Play();
        }
    }

    void OnDisable()
    {
        audioSource.Stop();
    }
}
