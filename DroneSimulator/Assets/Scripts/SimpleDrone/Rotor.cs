using System.Collections;
using System.Collections.Generic;
using UnityEditor.EditorTools;
using UnityEngine;

[RequireComponent(typeof(BaseModule))]
public class Rotor : MonoBehaviour
{
    [Tooltip("Maximum force the rotor can produce in Newtons")]
    [field: SerializeField] 
    public float MaxForce { get; private set; } = 1000;
    [Tooltip("Direction the rotor rotates in. 1 for clockwise, -1 for counter-clockwise")]
    [field: SerializeField]
    public float RotationDirection { get; private set; } = 1;

    // [Tooltip("Maximum Power the rotor needs to produce the maximum force in Watts")]
    // [field: SerializeField]
    // public float MaxPower { get; private set; } = 100;

    private DronePhysics dronePhysics;
    private AudioSource audioSource;

    void OnEnable()
    {
        dronePhysics = GetComponentInParent<DronePhysics>();
        audioSource = GetComponent<AudioSource>();
        Invoke("StartSound", Random.Range(0, 20));
    }

    private void StartSound()
    {
        audioSource.Play();
    }

    void OnDisable()
    {
        dronePhysics = null;
    }

    public void Throttle(float throttle)
    {
        throttle = Mathf.Clamp01(throttle);
        Vector3 force = transform.up * throttle * MaxForce;
        dronePhysics.AddForce(force, transform.position);
        dronePhysics.AddTorque(force * RotationDirection);
        transform.GetChild(0).localScale = Vector3.one * throttle * 0.99f;
        audioSource.volume = throttle;
    }
}
