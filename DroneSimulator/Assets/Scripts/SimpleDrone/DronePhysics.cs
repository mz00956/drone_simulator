using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class DronePhysics : MonoBehaviour
{
    private Rigidbody droneRigidbody;
    public Vector3 LocalDroneCoM { 
        get {
            return droneRigidbody.centerOfMass;
        }
        private set {
            droneRigidbody.centerOfMass = value;
        }
    }

    public Vector3 WorldDroneCoM {
        get {
            return droneRigidbody.worldCenterOfMass;
        }
    }

    public float Mass {
        get {
            return droneRigidbody.mass;
        }
        private set {
            droneRigidbody.mass = value;
        }
    }

    [SerializeField]
    private bool debug = true;

    private List<ModuleBase> modules = new List<ModuleBase>();
    private List<Rotor> rotors = new List<Rotor>();

    void OnEnable()
    {
        droneRigidbody = GetComponent<Rigidbody>();
        modules.AddRange(GetComponentsInChildren<ModuleBase>());
        rotors.AddRange(GetComponentsInChildren<Rotor>());
        UpdateRigidbody();
    }

    void FixedUpdate()
    {
        modules.Clear();
        modules.AddRange(GetComponentsInChildren<ModuleBase>());
        UpdateRigidbody();
    }

    [ContextMenu("Update Rigidbody")]
    private void UpdateRigidbody()
    {
        var (totalMass, CoM) = CalculateMassAndCenterOfMass(modules);
        Mass = totalMass;
        LocalDroneCoM = CoM;

        Matrix4x4 droneInertiaTensor = CalculateTotalInertiaTensor(modules, LocalDroneCoM);
        Vector3 momentOfInertia = new(droneInertiaTensor[0, 0], droneInertiaTensor[1, 1], droneInertiaTensor[2, 2]);

        droneRigidbody.inertiaTensor = momentOfInertia;
        droneRigidbody.inertiaTensorRotation = Quaternion.identity;
    }

    private (float totalMass, Vector3 CoM) CalculateMassAndCenterOfMass(List<ModuleBase> modules)
    {
        Vector3 totalCoM = Vector3.zero;
        float totalMass = 0f;

        foreach (var module in modules)
        {
            totalCoM += (module.transform.localPosition + module.CoM) * module.Mass;
            totalMass += module.Mass;
        }

        return (totalMass, totalCoM / totalMass);
    }

    private Matrix4x4 CalculateTotalInertiaTensor(List<ModuleBase> modules,Vector3 centerOfMass)
    {
        Matrix4x4 totalInertiaTensor = Matrix4x4.zero;

        foreach (var module in modules)
        {
            //relative position of module to center of mass
            Vector3 r = module.transform.localPosition + module.CoM - centerOfMass;
            //mass of module
            float m = module.Mass;

            //GPT Magic:

            // Diagonal elements
            totalInertiaTensor[0, 0] += m * (r.y * r.y + r.z * r.z); // Ixx
            totalInertiaTensor[1, 1] += m * (r.x * r.x + r.z * r.z); // Iyy
            totalInertiaTensor[2, 2] += m * (r.x * r.x + r.y * r.y); // Izz

            // Off-diagonal elements (assuming symmetric mass distribution)
            totalInertiaTensor[0, 1] -= m * r.x * r.y; // Ixy
            totalInertiaTensor[1, 0] -= m * r.x * r.y; // Iyx

            totalInertiaTensor[0, 2] -= m * r.x * r.z; // Ixz
            totalInertiaTensor[2, 0] -= m * r.x * r.z; // Izx

            totalInertiaTensor[1, 2] -= m * r.y * r.z; // Iyz
            totalInertiaTensor[2, 1] -= m * r.y * r.z; // Izy
        }

        return totalInertiaTensor;
    }

    void OnDisable()
    {
        modules.Clear();
    }

    public void AddModule(ModuleBase module)
    {
        modules.Add(module);
        UpdateRigidbody();
    }

    public void RemoveModule(ModuleBase module)
    {
        modules.Remove(module);
        UpdateRigidbody();
    }

    public void AddForce(Vector3 force, Vector3 position)
    {

        droneRigidbody.AddForceAtPosition(force, position, ForceMode.Force);

        RigidBodyDebugger rigidBodyDebugger = GetComponent<RigidBodyDebugger>();
        if (rigidBodyDebugger != null)
        {
            rigidBodyDebugger.AddGlobalForce(force, position);
        }
    }

    public void AddTorque(Vector3 torque)
    {
        droneRigidbody.AddTorque(torque, ForceMode.Force);

        RigidBodyDebugger rigidBodyDebugger = GetComponent<RigidBodyDebugger>();
        if (rigidBodyDebugger != null)
        {
            rigidBodyDebugger.AddGlobalTorque(torque, droneRigidbody.worldCenterOfMass);
        }
    }

    public void Update()
    {
        if (debug)
        {
            Debug.DrawRay(transform.TransformPoint(LocalDroneCoM), transform.forward, Color.blue, 0, false);
            Debug.DrawRay(transform.TransformPoint(LocalDroneCoM), transform.up, Color.green, 0, false);
            Debug.DrawRay(transform.TransformPoint(LocalDroneCoM), transform.right, Color.red, 0, false);
        }
    }

    public float getGravitationalForce()
    {
        return droneRigidbody.mass * Physics.gravity.y;
    }

    public float getMaxThrust()
    {
        float maxThrust = 0;
        foreach (Rotor rotor in rotors)
        {
            maxThrust += rotor.MaxForce;
        }
        return maxThrust;
    }

    public float getVerticalVelocity()
    {
        return Vector3.Dot(droneRigidbody.velocity, transform.up);
    }
}
