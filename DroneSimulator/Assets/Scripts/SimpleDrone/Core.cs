using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Core : MonoBehaviour
{
    private InputTestScene_Actions inputActions;
    public int controllerIndex = 0;

    private List<Rotor> rotors;
    private DronePhysics dronePhysics;

    [SerializeField]
    private float yawSensitivity = 0.25f;
    [SerializeField]
    private float pitchSensitivity = 0.25f;
    [SerializeField]
    private float rollSensitivity = 0.25f;

    [SerializeField]
    private ThrustPriority thrustPriority = ThrustPriority.ClampThrottle;

    private enum ThrustPriority
    {
        ClampThrottle,
        PrioritizeThrust,
        PrioritizeTorque
    }

    private float lastThrottle = 0;
    private bool additiveThrottle = false;

    [SerializeField]
    private bool altitudeControl = false;
    [SerializeField]
    private float altitudeControlSensitivity = 10f;
    [SerializeField]
    private float altitudeControlVelocitySensitivity = 0.5f;
    [SerializeField]
    private float altitudeControlMinThrottle = 0.1f;

    [SerializeField]
    private float desiredAltitude = 0;
    [SerializeField]
    private float maxAltitude = 2;    

    private float throttleInput;
    private float yaw;
    private float pitch;
    private float roll;
    private bool buttonSouth3;
    private bool Back2;

    void OnEnable()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();

        rotors = new List<Rotor>(transform.parent.GetComponentsInChildren<Rotor>());
        dronePhysics = transform.GetComponentInParent<DronePhysics>();
    }

    void OnDisable()
    {
        inputActions.Disable();
    }

    List<float> measuredAltitudes = new List<float>();

    void Update()
    {
        ReadInput();

        ProcessesInput();
    }

    private void ReadInput()
    {
        switch (controllerIndex)
        {
            default:
            throttleInput = inputActions.DroneControl.Throttle.ReadValue<float>();
            yaw = inputActions.DroneControl.RotateY.ReadValue<float>();
            pitch = inputActions.DroneControl.RotateX.ReadValue<float>();
            roll = inputActions.DroneControl.RotateZ.ReadValue<float>();
            Back2 = inputActions.DroneControl.Back2.triggered;
            buttonSouth3 = inputActions.DroneControl.ButtonSouth3.triggered;
            break;
            case 1:
            throttleInput = inputActions.DroneControl1.Throttle.ReadValue<float>();
            yaw = inputActions.DroneControl1.RotateY.ReadValue<float>();
            pitch = inputActions.DroneControl1.RotateX.ReadValue<float>();
            roll = inputActions.DroneControl1.RotateZ.ReadValue<float>();
            Back2 = inputActions.DroneControl1.Back2.triggered;
            buttonSouth3 = inputActions.DroneControl1.ButtonSouth3.triggered;
            break;
            case 2:
            throttleInput = inputActions.DroneControl2.Throttle.ReadValue<float>();
            yaw = inputActions.DroneControl2.RotateY.ReadValue<float>();
            pitch = inputActions.DroneControl2.RotateX.ReadValue<float>();
            roll = inputActions.DroneControl2.RotateZ.ReadValue<float>();
            Back2 = inputActions.DroneControl2.Back2.triggered;
            buttonSouth3 = inputActions.DroneControl2.ButtonSouth3.triggered;
            break;
        }


    }

    private void ProcessesInput()
    {
        if (Back2)
        {
            dronePhysics.gameObject.transform.position += Vector3.up * 10;
            dronePhysics.gameObject.transform.rotation = Quaternion.identity;

            dronePhysics.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
            dronePhysics.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }

        if (buttonSouth3)
        {
            additiveThrottle = !additiveThrottle;
        }

        if (additiveThrottle)
        {
            throttleInput = Mathf.Clamp01(throttleInput * 0.05f + lastThrottle);
        }

        if (altitudeControl)
        {
            AltitudeControl();
        }
    }

    private void AltitudeControl()
    {
        desiredAltitude = maxAltitude * throttleInput;

        int layerMask = 1 << 8;
        layerMask = ~layerMask;

        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, maxAltitude + 1, layerMask))
        {
            float altitude = hit.distance;
            measuredAltitudes.Add(altitude);
            if (measuredAltitudes.Count > 3)
                measuredAltitudes.RemoveAt(0);
        }
        else
        {
            measuredAltitudes.Add(maxAltitude + 1);
            if (measuredAltitudes.Count > 3)
                measuredAltitudes.RemoveAt(0);
        }

        float averageAltitude = 0;
        foreach (float altitude in measuredAltitudes)
        {
            averageAltitude += altitude;
        }
        averageAltitude /= measuredAltitudes.Count;

        float altitudeError = desiredAltitude - averageAltitude;
        float neededForceToHover = -dronePhysics.getGravitationalForce();
        float maxForce = dronePhysics.getMaxThrust();
        float verticalVelocity = dronePhysics.getVerticalVelocity();

        float forceAdjustment = altitudeError;
        forceAdjustment *= altitudeControlSensitivity;

        float velocityAdjustment = verticalVelocity / Mathf.Max(Mathf.Abs(altitudeError), 0.001f);
        velocityAdjustment *= altitudeControlVelocitySensitivity;

        float neededForce = Mathf.Max(neededForceToHover + forceAdjustment - velocityAdjustment, 0);

        Vector3 neededForceVector = Vector3.up * neededForce;

        Vector3 upProjection = Vector3.Project(transform.up, Vector3.up);
        float upProjectionMagnitude = upProjection.magnitude;

        Vector3 neededProjectedForceVector = transform.up * neededForceVector.magnitude / upProjectionMagnitude;

        float neededForceMagnitude = neededProjectedForceVector.magnitude;

        float neededThrottle = neededForceMagnitude / maxForce;

        neededThrottle = Mathf.Max(neededThrottle, altitudeControlMinThrottle);

        throttleInput = Mathf.Clamp01(neededThrottle);

        Debug.Log("Throttle: " + throttleInput + " Altitude: " + averageAltitude + "Deviation: " + altitudeError);
    }

    void FixedUpdate()
    {
        List<float> rawRotorThrottles = RawThrottleChange(throttleInput, dronePhysics.LocalDroneCoM);

        string debugOutput1 = "";
        string debugOutput2 = "";

        debugOutput1 += "rawRotorThrottles:\n" + string.Join(", ", rawRotorThrottles.Select(x => x.ToString())) + "\n";
        debugOutput2 += "Desired Force:\n" + throttleInput * dronePhysics.getMaxThrust() + "\n";

        float highest = Mathf.Max(rawRotorThrottles.ToArray());
        float lowest = Mathf.Min(rawRotorThrottles.ToArray());

        List<float> rotorThrottles = rawRotorThrottles;

        if (lowest < 0 || highest > 1) 
            switch (thrustPriority)
            {
                case ThrustPriority.PrioritizeThrust:
                    rotorThrottles = PrioritizeThrottle(rawRotorThrottles, lowest, highest);
                    rotorThrottles = RedistributeTo01(rotorThrottles);
                    break;
                case ThrustPriority.PrioritizeTorque:
                    rotorThrottles = PrioritizeTorque(rawRotorThrottles, lowest, highest);
                    break;
            }

        debugOutput1 += "processedRotorThrottles:\n" + string.Join(", ", rotorThrottles.Select(x => x.ToString())) + "\n";

        float averageThrottle = 0;
        foreach (float throttle in rotorThrottles)
        {
            averageThrottle += throttle;
        }
        averageThrottle /= rotorThrottles.Count;
        debugOutput2 += "Attempted Force:\n" + averageThrottle * dronePhysics.getMaxThrust() + "\n";

        rotorThrottles = Clamped01Throttle(rotorThrottles);
        
        debugOutput1 += "clampedRotorThrottles:\n" + string.Join(", ", rotorThrottles.Select(x => x.ToString())) + "\n";

        averageThrottle = 0;
        foreach (float throttle in rotorThrottles)
        {
            averageThrottle += throttle;
        }
        averageThrottle /= rotorThrottles.Count;
        debugOutput2 += "Actual Force:\n" + averageThrottle * dronePhysics.getMaxThrust() + "\n";

        Debug.Log(debugOutput1);
        Debug.Log(debugOutput2);
        

        for (int i = 0; i < rotors.Count; i++)
        {
            rotors[i].Throttle(rotorThrottles[i]);
        }

        lastThrottle = throttleInput;
        transform.GetChild(0).localScale = Vector3.one * lastThrottle * 0.99f;
    }

    private List<float> RawThrottleChange(float throttleInput, Vector3 COMPosition)
    {
        List<float> rotorThrottles = new List<float>();

        for (int i = 0; i < rotors.Count; i++)
        {
            float rotorThrottle = throttleInput;
            if (rotors[i].RotationDirection == 1)
                rotorThrottle += yaw * yawSensitivity / 2;
            else
                rotorThrottle -= yaw * yawSensitivity / 2;


            if (rotors[i].transform.localPosition.z > COMPosition.z)
            {
                rotorThrottle -= pitch * pitchSensitivity / 2;
            }
            else if (rotors[i].transform.localPosition.z < COMPosition.z)
            {
                rotorThrottle += pitch * pitchSensitivity / 2;
            }

            if (rotors[i].transform.localPosition.x > COMPosition.x)
            {
                rotorThrottle += roll * rollSensitivity / 2;
            }
            else if (rotors[i].transform.localPosition.x < COMPosition.x)
            {
                rotorThrottle -= roll * rollSensitivity / 2;
            }

            rotorThrottles.Add(rotorThrottle);
        }

        return rotorThrottles;
    }

    private List<float> Clamped01Throttle(List<float> rawThrottle)
    {
        List<float> clampedThrottle = new List<float>();

        for (int i = 0; i < rawThrottle.Count; i++)
        {
            float throttle = rawThrottle[i];
            if (throttle > 1)
            {
                throttle = 1;
            }
            else if (throttle < 0)
            {
                throttle = 0;
            }
            clampedThrottle.Add(throttle);
        }

        return clampedThrottle;
    }

    private List<float> PrioritizeThrottle(List<float> rawThrottle, float lowest, float highest)
    {
        List<float> clampedThrottle = new List<float>();
        
        float sumNegative = 0;
        foreach (var throttle in rawThrottle.Where(throttle => throttle < 0))
        {
            sumNegative += throttle;
        }
        float sumRange = 0;
        foreach (var throttle in rawThrottle.Where(throttle => throttle > 0 && throttle < 1))
        {
            sumRange += throttle;
        }
        float sumPositive = 0;
        foreach (var throttle in rawThrottle.Where(throttle => throttle > 1))
        {
            sumPositive += throttle - 1;
        }

        if (lowest < 0 && highest <= 1)
        {
            float ratio = sumNegative / sumRange;
            for (int i = 0; i < rawThrottle.Count; i++)
            {
                float throttle = rawThrottle[i];
                if (throttle <= 0)
                {
                    throttle = 0;
                }
                else if (throttle > 0)
                {
                    throttle += ratio * throttle;
                }
                clampedThrottle.Add(throttle);
            }
        }
        else if (lowest >= 0 && highest > 1)
        {
            float ratio = sumPositive / sumRange;
            for (int i = 0; i < rawThrottle.Count; i++)
            {
                float throttle = rawThrottle[i];
                if (throttle >= 1)
                {
                    throttle = 1;
                }
                else if (throttle < 1)
                {
                    throttle += ratio * throttle;
                }
                clampedThrottle.Add(throttle);
            }
        }
        else if (lowest < 0 && highest > 1)
        {
            float ratio = (sumNegative + sumPositive) / sumRange;
            for (int i = 0; i < rawThrottle.Count; i++)
            {
                float throttle = rawThrottle[i];
                if (throttle <= 0)
                {
                    throttle = 0;
                }
                else if (throttle >= 1)
                {
                    throttle = 1;
                }
                else
                {
                    throttle += ratio * throttle;
                }
                clampedThrottle.Add(throttle);
            }
        }

        return clampedThrottle;
    }

    private List<float> PrioritizeTorque(List<float> rawThrottle, float lowest, float highest)
    {
        List<float> clampedThrottle = new();
        
        float throttleRange = highest - lowest;
        float positiveOffset = Mathf.Max(0, highest - 1);
        float negativeOffset = Mathf.Min(0, lowest);

        float offset = 0;

        if (throttleRange <= 1)
        {
            offset = positiveOffset > 0 ? positiveOffset : negativeOffset;
        }
        else
        {
            offset = (positiveOffset + negativeOffset) / 2;
        }

        return rawThrottle.Select(x => x - offset).ToList();
    }

    private List<float> RedistributeTo01(List<float> throttles)
    {
        float sum = throttles.Sum();
        int count = throttles.Count;

        float sumAbove1 = throttles.Where(x => x > 1).Select(x => x - 1).Sum();
        float sumBelow0 = throttles.Where(x => x < 0).Sum();

        //Clamp to 0-1
        for (int i = 0; i < throttles.Count; i++)
        {
            throttles[i] = Mathf.Clamp01(throttles[i]);
        }

        float sumToRedistribute = sumAbove1 + sumBelow0;

        if (sum >= count)
        {
            return throttles.Select(x => 1f).ToList();
        }

        if (sum <= 0)
        {
            return throttles.Select(x => 0f).ToList();
        }

        if (sumToRedistribute == 0)
        {
            return throttles;
        }

        if (sumToRedistribute > 0)
        {
            // Get all where we can add
            List<(int index, float value)> throttlesBelow1 = new();
            for (int i = 0; i < throttles.Count; i++)
            {
                if (throttles[i] < 1)
                { 
                    throttlesBelow1.Add((i, throttles[i]));
                }
            }

            // Sort by value descending
            throttlesBelow1 = throttlesBelow1.OrderByDescending(x => x.value).ToList();

            // Evenly distribute
            while (sumToRedistribute > 0 && throttlesBelow1.Count > 0)
            {
                float toAdd = sumToRedistribute / throttlesBelow1.Count;

                if (throttlesBelow1[0].value + toAdd <= 1)
                {
                    for (int i = 0; i < throttlesBelow1.Count; i++)
                    {
                        throttles[throttlesBelow1[i].index] += toAdd;
                        Debug.Log("Added " + toAdd + " to " + throttlesBelow1[i].index);
                    }
                    return throttles;
                }

                sumToRedistribute -= 1 - throttlesBelow1[0].value;
                throttles[throttlesBelow1[0].index] = 1;
                throttlesBelow1.RemoveAt(0);
            }
        }
        else
        {
            // Get all where we can subtract
            List<(int index, float value)> throttlesAbove0 = new();
            for (int i = 0; i < throttles.Count; i++)
            {
                if (throttles[i] > 0)
                {
                    throttlesAbove0.Add((i, throttles[i]));
                }
            }

            // Sort by value ascending
            throttlesAbove0 = throttlesAbove0.OrderBy(x => x.value).ToList();

            // Evenly distribute
            while (sumToRedistribute < 0 && throttlesAbove0.Count > 0)
            {
                float toSubtract = sumToRedistribute / throttlesAbove0.Count;

                if (throttlesAbove0[0].value + toSubtract >= 0)
                {
                    for (int i = 0; i < throttlesAbove0.Count; i++)
                    {
                        throttles[throttlesAbove0[i].index] += toSubtract;
                    }
                    return throttles;
                }

                sumToRedistribute -= throttlesAbove0[0].value;
                throttles[throttlesAbove0[0].index] = 0;
                throttlesAbove0.RemoveAt(0);
            }
        }

        return null;
    }
}
