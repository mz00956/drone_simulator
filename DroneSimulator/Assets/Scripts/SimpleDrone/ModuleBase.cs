using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleBase : MonoBehaviour
{
    [field: SerializeField]
    public float Mass { get; private set; } = 1;
    [field: SerializeField]
    public Vector3 CoM { get; private set; } = Vector3.zero;
}
