using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTestScene_HatTest : MonoBehaviour
{
    public InputTestScene_Actions inputActions;
    [SerializeField]
    private List<Transform> HatVisualizers3;
    [SerializeField]
    private List<Transform> HatVisualizers2;
    [SerializeField]
    private List<Transform> HatVisualizers1;

    [SerializeField]
    private Material selectionMaterial;
    [SerializeField]
    private Material defaultMaterial;

    [SerializeField]
    private Transform hatXVisualizer;
    [SerializeField]
    private Transform hatZVisualizer;

    private Vector2 hatSelection = new Vector2(0, 0);


    private List<List<Transform>> hatVisualizers;
    // Start is called before the first frame update
    void Start()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();

        hatVisualizers = new List<List<Transform>>
        {
            HatVisualizers3,
            HatVisualizers2,
            HatVisualizers1
        };

        for (int i = 0; i < hatVisualizers.Count; i++)
        {
            for (int j = 0; j < hatVisualizers[i].Count; j++)
            {
                hatVisualizers[i][j].GetComponent<MeshRenderer>().material = defaultMaterial;
            }
        }

        hatVisualizers[0][0].GetComponent<MeshRenderer>().material = selectionMaterial;
    }

    // Update is called once per frame
    void Update()
    {
        hatXVisualizer.localPosition = new Vector3(inputActions.LogitechExtreme3DPro.HatKnobX.ReadValue<float>(), 0, 0);
        hatZVisualizer.localPosition = new Vector3(0, inputActions.LogitechExtreme3DPro.HatKnobZ.ReadValue<float>(), 0);

        if (inputActions.LogitechExtreme3DPro.HatKnobBack.triggered)
        {
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = defaultMaterial;
            hatSelection.y = Mathf.Clamp(hatSelection.y + 1, 0, 2);
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = selectionMaterial;
        }

        if (inputActions.LogitechExtreme3DPro.HatKnobForward.triggered)
        {
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = defaultMaterial;
            hatSelection.y = Mathf.Clamp(hatSelection.y - 1, 0, 2);
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = selectionMaterial;
        }

        if (inputActions.LogitechExtreme3DPro.HatKnobLeft.triggered)
        {
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = defaultMaterial;
            hatSelection.x = Mathf.Clamp(hatSelection.x - 1, 0, 4);
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = selectionMaterial;
        }

        if (inputActions.LogitechExtreme3DPro.HatKnobRight.triggered)
        {
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = defaultMaterial;
            hatSelection.x = Mathf.Clamp(hatSelection.x + 1, 0, 4);
            hatVisualizers[(int)hatSelection.y][(int)hatSelection.x].GetComponent<MeshRenderer>().material = selectionMaterial;
        }
    }

    private void OnEnable()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }
}
