using UnityEngine;

public class InputTestScene_StickAxisTest : MonoBehaviour
{
    public InputTestScene_Actions inputActions;
    public Transform throttleVisualizer;

    public Transform triggerVisualizer;
    private float speed = 20;
    // Start is called before the first frame update
    void Start()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 rotation = transform.rotation.eulerAngles;
        rotation.y += inputActions.LogitechExtreme3DPro.StickY.ReadValue<float>();
        rotation.x += inputActions.LogitechExtreme3DPro.StickX.ReadValue<float>();
        rotation.z += inputActions.LogitechExtreme3DPro.StickZ.ReadValue<float>();
        transform.rotation = Quaternion.Euler(rotation);

        throttleVisualizer.localPosition = new Vector3(0, 0, inputActions.LogitechExtreme3DPro.ThrottleAxis.ReadValue<float>());

        if (inputActions.LogitechExtreme3DPro.StickButton1.triggered || triggerVisualizer.localPosition.z > 0)
        {
            triggerVisualizer.localPosition = new Vector3(0, 0, triggerVisualizer.localPosition.z + Time.deltaTime * speed);
        }
        
        if (inputActions.LogitechExtreme3DPro.StickButton2.triggered)
        {
            triggerVisualizer.localPosition = new Vector3(0, 0, 0);
        }
    }

    private void OnEnable()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }
}