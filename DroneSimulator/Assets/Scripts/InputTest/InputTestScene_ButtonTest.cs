using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTestScene_ButtonTest : MonoBehaviour
{
public InputTestScene_Actions inputActions;

    public List<Transform> buttonVisualizers;

    public Material selectionMaterial;
    public Material defaultMaterial;

    // Start is called before the first frame update
    void Start()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();
    }

    // Update is called once per frame
    void Update()
    {
        if (inputActions.LogitechExtreme3DPro.StickButton1.ReadValue<float>() > 0.5f)
            buttonVisualizers[0].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[0].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton2.ReadValue<float>() > 0.5f)
            buttonVisualizers[1].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[1].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton3.ReadValue<float>() > 0.5f)
            buttonVisualizers[2].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[2].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton4.ReadValue<float>() > 0.5f)
            buttonVisualizers[3].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[3].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton5.ReadValue<float>() > 0.5f)
            buttonVisualizers[4].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[4].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton6.ReadValue<float>() > 0.5f)
            buttonVisualizers[5].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[5].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton7.ReadValue<float>() > 0.5f)
            buttonVisualizers[6].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[6].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton8.ReadValue<float>() > 0.5f)
            buttonVisualizers[7].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[7].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton9.ReadValue<float>() > 0.5f)
            buttonVisualizers[8].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[8].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton10.ReadValue<float>() > 0.5f)
            buttonVisualizers[9].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[9].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton11.ReadValue<float>() > 0.5f)
            buttonVisualizers[10].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[10].GetComponent<MeshRenderer>().material = defaultMaterial;

        if (inputActions.LogitechExtreme3DPro.StickButton12.ReadValue<float>() > 0.5f)
            buttonVisualizers[11].GetComponent<MeshRenderer>().material = selectionMaterial;
        else
            buttonVisualizers[11].GetComponent<MeshRenderer>().material = defaultMaterial;
    }

    private void OnEnable()
    {
        inputActions ??= new InputTestScene_Actions();
        inputActions.Enable();
    }

    private void OnDisable()
    {
        inputActions.Disable();
    }
}
