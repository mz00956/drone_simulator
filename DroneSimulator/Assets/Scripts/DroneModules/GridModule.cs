using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class GridModule : MonoBehaviour
{
    [Header("Grid Module")]
    [field: SerializeField]
    protected Vector3Int gridPosition = Vector3Int.zero;
    
    [field: SerializeField]
    protected int layer = 0;

    public void SetGridModule(Transform parent, Vector3Int localPosition)
    {
        transform.SetParent(parent);       
        if (transform.GetComponentInParent<GridModuleHolder>() == null)
        {
            Debug.LogError("Every Module must be a child of a GridModuleHolder", this);
            enabled = false;
            transform.name = $"{GetModuleName()} {localPosition} - ERROR";
            return;
        }

        transform.SetLocalPositionAndRotation(localPosition, Quaternion.identity);
        transform.localScale = Vector3.one;
        transform.name = $"{GetModuleName()} {localPosition}";
        gridPosition = localPosition;
        layer = (int)Mathf.Log(parent.localScale.x, 2);
    }

    public abstract string GetModuleName();
}