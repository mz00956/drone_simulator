using UnityEngine;

public class BaseModule : GridModule
{
    [field: Header("Base Module")]
    [field: SerializeField]
    protected float Mass { get; set; } = 1;
    [field: SerializeField]
    protected Vector3 LocalCenterOfMass { get; set; } = Vector3.zero;

    public override string GetModuleName()
    {
        return $"BaseModule";
    }
}
