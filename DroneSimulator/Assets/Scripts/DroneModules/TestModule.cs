using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestModule : BaseModule
{
    [SerializeField]
    private GridModuleHolder gridModuleHolder = null;

    [SerializeField]
    private Material validPlacementMaterial = null;

    [SerializeField]
    private Material invalidPlacementMaterial = null;

    void Update()
    {
        if (gridModuleHolder == null)
        {
            return;
        }

        layer = Mathf.Max(0, Mathf.Min(layer, gridModuleHolder.Layers - 1));

        transform.SetParent(gridModuleHolder.transform);
        transform.localPosition -= transform.localScale / 2f;
        transform.localPosition /= (int)Mathf.Pow(2, layer);
        transform.localScale = Vector3.one * (int)Mathf.Pow(2, layer);

        transform.localPosition = Vector3Int.RoundToInt(transform.localPosition);

        if (!gridModuleHolder.IsOccupied(Vector3Int.RoundToInt(transform.localPosition), layer))
        {
            GetComponentInChildren<Renderer>().material = validPlacementMaterial;
        }
        else
        {
            GetComponentInChildren<Renderer>().material = invalidPlacementMaterial;
        }

        transform.localPosition *= (int)Mathf.Pow(2, layer);
        transform.localPosition += transform.localScale / 2f;
    }
}
