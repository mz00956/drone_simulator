using System.Collections.Generic;
using UnityEngine;

public class SubGridModule : GridModule
{
    [Header("SubGridModule")]
    public int subSlotsOccupied = 0;

    public override string GetModuleName()
    {
        return $"SubGridModule";
    }
}
