using UnityEngine;

public class CoreModule : BaseModule
{
    public new float Mass {
        get {
            return base.Mass;
        }
        private set {
            base.Mass = value;
        }
    }

    public new Vector3 LocalCenterOfMass {
        get {
            return base.LocalCenterOfMass;
        }
        private set {
            base.LocalCenterOfMass = value;
        }
    }

    public override string GetModuleName()
    {
        return $"CoreModule";
    }
}