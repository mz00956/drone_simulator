using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class GridModuleHolder : MonoBehaviour
{
    [ShowInInspector, ReadOnly]
    private Dictionary<Vector3Int, GridModule>[] grids;
    [ShowInInspector, ReadOnly]
    private GameObject[] gridObjects;

    [Header("Grid Module Holder")]
    [SerializeField]
    public int Layers = 3;
    [SerializeField]
    private GameObject subGridModulePrefab = null;

    void OnEnable()
    {
        grids = new Dictionary<Vector3Int, GridModule>[Layers];
        gridObjects = new GameObject[Layers];
        for (int i = 0; i < Layers; i++)
        {
            grids[i] = new Dictionary<Vector3Int, GridModule>();
            int gridSize = (int)Mathf.Pow(2, i);
            gridObjects[i] = new GameObject($"Grid {gridSize}");
            gridObjects[i].transform.SetParent(transform);
            gridObjects[i].transform.localRotation = Quaternion.identity;
            gridObjects[i].transform.localScale = Vector3.one * gridSize;
            gridObjects[i].transform.localPosition = Vector3.one * gridSize / 2f;
        }
    }


    [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
    public bool TryAddModule(BaseModule module, Vector3Int gridPosition, int layer)
    {
        if (layer >= Layers)
        {
            Debug.LogWarning("Layer is too high", this);
            return false;
        }

        if (IsInAnyGrid(module))
        {
            bool isInThisGrid = IsInThisGrid(module);
            Debug.LogWarning($"Module is already in {(isInThisGrid ? "this" : "a different")} grid", this);
            return false;
        }

        if (IsOccupied(gridPosition, layer))
        {
            Debug.LogWarning("Position is already occupied", this);
            return false;
        }

        module.SetGridModule(gridObjects[layer].transform, gridPosition);
        grids[layer].Add(gridPosition, module);

        while (layer < Layers - 1)
        {
            layer++;
            gridPosition = convertToNextLargerLayer(gridPosition);


            Debug.Log("Adding subgrid if needed at " + gridPosition + " in layer " + layer);
            if (grids[layer].ContainsKey(gridPosition))
            {
                Debug.Log("Subgrid already exists");
                SubGridModule subGrid = grids[layer][gridPosition] as SubGridModule;
                subGrid.subSlotsOccupied++;
                break;
            }
            else
            {
                Debug.Log("Creating new subgrid");
                SubGridModule subGrid;
                if (subGridModulePrefab != null)
                    subGrid = Instantiate(subGridModulePrefab, transform).AddComponent<SubGridModule>();
                else
                    subGrid = new GameObject($"New GameObject").AddComponent<SubGridModule>();
                subGrid.SetGridModule(gridObjects[layer].transform, gridPosition);
                subGrid.subSlotsOccupied++;
                grids[layer].Add(gridPosition, subGrid);
            }
        }
        
        return true;
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
    public void RemoveModule(BaseModule module)
    {
        if (!IsInThisGrid(module))
        {
            Debug.LogWarning("Module is not in this grid", this);
            return;
        }

        Vector3Int gridPosition = Vector3Int.RoundToInt(module.transform.localPosition);
        int layer = (int)Mathf.Log(module.transform.parent.localScale.x, 2);

        grids[layer].Remove(gridPosition);

        while (layer < Layers - 1)
        {
            layer++;
            gridPosition = convertToNextLargerLayer(gridPosition);

            if (grids[layer].ContainsKey(gridPosition))
            {
                SubGridModule subGrid = grids[layer][gridPosition] as SubGridModule;
                subGrid.subSlotsOccupied--;
                if (subGrid.subSlotsOccupied > 0)
                {
                    break;
                }
                grids[layer].Remove(gridPosition);
                Destroy(subGrid.gameObject);
            }
        }

        module.transform.SetParent(null);
        module.transform.localScale = Vector3.one;
        module.transform.localRotation = Quaternion.identity;
        module.transform.position = Vector3.zero;
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
    public bool IsInAnyGrid(BaseModule module)
    {
        return module.GetComponentInParent<GridModuleHolder>() != null;
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
    public bool IsInThisGrid(BaseModule module)
    {
        return module.GetComponentInParent<GridModuleHolder>() == this;
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
    public (int layer, Vector3Int gridPosition) GetGridPosition(BaseModule module)
    {
        if (!IsInThisGrid(module))
        {
            Debug.LogWarning("Module is not in this grid", this);
            return (-1, Vector3Int.zero);
        }

        Vector3Int gridPosition = Vector3Int.RoundToInt(module.transform.localPosition);
        int layer = (int)Mathf.Log(module.transform.parent.localScale.x, 2);

        return (layer, gridPosition);
    }

    [Button(ButtonSizes.Medium, ButtonStyle.Box, Expanded = true)]
    public bool IsOccupied(Vector3Int gridPosition, int layer)
    {
        if (grids[layer].ContainsKey(gridPosition))
        {
            return true;
        }

        while (layer < Layers -1)
        {
            layer++;
            gridPosition = convertToNextLargerLayer(gridPosition);

            if (grids[layer].ContainsKey(gridPosition))
            {
                GridModule module = grids[layer][gridPosition];

                if (module is BaseModule)
                {
                    return true;
                }
                
                if (module is SubGridModule)
                {
                    return false;
                }
            }
            
        }
        return false;
    }

    private Vector3Int convertToNextLargerLayer(Vector3Int gridPosition)
    {
        Vector3Int newGridPosition = gridPosition /2;

        if (gridPosition.x < 0)
        {
            newGridPosition.x -= 1;
        }

        if (gridPosition.y < 0)
        {
            newGridPosition.y -= 1;
        }

        if (gridPosition.z < 0)
        {
            newGridPosition.z -= 1;
        }

        return newGridPosition;
    }
}