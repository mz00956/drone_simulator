using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class RigidBodyDebugger : MonoBehaviour
{
    [SerializeField]
    private bool lockPosition = false;
    [SerializeField]
    private bool lockRotation = false;
    [SerializeField]
    private bool updateLocks = false;

    [SerializeField]
    private float newtonPerMeter = 10f;
    [SerializeField]
    private ShowForce forceSettings = ShowForce.Force;

    [SerializeField]
    private float newtonMeterPer2PI = 10f;
    [SerializeField]
    private float torqueRadius_M = 0.4f;
    [SerializeField]
    private float circleSeparation = 0.1f;
    [SerializeField]
    private ShowTorque torqueSettings = ShowTorque.Torque;

    [SerializeField]
    private List<TestValues> testValues;

    private Rigidbody rb;
    private List<(Vector3 force, Vector3 position, Color color)> forces = new List<(Vector3, Vector3, Color)>();
    private List<(Vector3 torque, Vector3 position, Color color)> torques = new List<(Vector3, Vector3, Color)>();

    private Vector3 fixedUpdateOffset = Vector3.zero;

    [System.Serializable]
    public struct TestValues
    {
        public Vector3 force_N;
        public Vector3 torque_Nm;
        public Vector3 forceOffsetCOM;
    }

    [System.Flags]
    public enum ShowForce
    {
        None = 0,
        Force = 2,
        Force3Axis = 4,
        ForceSum = 8,
        Force3AxisSum = 16
    }

    [System.Flags]
    public enum ShowTorque
    {
        None = 0,
        Torque = 2,
        Torque3Axis = 4,
        TorqueSum = 8,
        Torque3AxisSum = 16
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        forces.Clear();
        torques.Clear();

        foreach (var testValue in testValues)
        {
            AddGlobalForce(testValue.force_N, Vector3.Scale(rb.worldCenterOfMass, rb.transform.lossyScale) + testValue.forceOffsetCOM);
            rb.AddForceAtPosition(testValue.force_N, rb.worldCenterOfMass + testValue.forceOffsetCOM, ForceMode.Force);
            AddGlobalTorque(testValue.torque_Nm, Vector3.Scale(rb.worldCenterOfMass, rb.transform.lossyScale) + testValue.forceOffsetCOM);   
            rb.AddTorque(testValue.torque_Nm, ForceMode.Force);
        }


        // if (rb.useGravity && rb.mass > 0f)
        //     forces.Add((rb.mass * Physics.gravity, rb.worldCenterOfMass, Color.red));

        string output = "";
        output += "rb.worldCenterOfMass: " + rb.worldCenterOfMass + "\n";
        output += "rb.centerOfMass: " + rb.centerOfMass + "\n";
        output += "rb.transform.position: " + rb.transform.position + "\n";
        output += "rb.transform.localScale: " + rb.transform.localScale + "\n";
        output += "rb.transform.lossyScale: " + rb.transform.lossyScale + "\n";
        Debug.Log(output);


        if (updateLocks)
        {
            if (lockPosition)
            {
                rb.position = transform.position;
                rb.velocity = Vector3.zero;
                rb.constraints |= RigidbodyConstraints.FreezePosition;
            }
            else
            {
                rb.constraints &= ~RigidbodyConstraints.FreezePosition;
            }

            if (lockRotation)
            {
                rb.rotation = transform.rotation;
                rb.angularVelocity = Vector3.zero;
                rb.constraints |= RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                rb.constraints &= ~RigidbodyConstraints.FreezeRotation;
            }
        }

        fixedUpdateOffset = rb.worldCenterOfMass;

        Debug.Log(forces.Count);
    }

    private void Update()
    {
        VisualizeForces(rb.worldCenterOfMass - fixedUpdateOffset);
        VisualizeTorques(rb.worldCenterOfMass - fixedUpdateOffset);
    }

    public void AddGlobalForce(Vector3 worldForce, Vector3 worldPosition)
    {
        if (worldPosition == rb.worldCenterOfMass)
        {
            forces.Add((worldForce, worldPosition, new Color(80, 240, 80)));
        }
        else
        {
            Vector3 offset = worldPosition - rb.worldCenterOfMass;

            forces.Add((worldForce, worldPosition, new Color(90, 255, 0)));
            torques.Add((Vector3.Cross(offset, worldForce * offset.magnitude), rb.worldCenterOfMass, new Color(90, 255, 0)));
        }
    }

    public void AddGlobalTorque(Vector3 worldTorque, Vector3 worldPosition)
    {
        torques.Add((worldTorque, worldPosition, new Color(0, 0, 255)));
    }

    private void VisualizeForces(Vector3 fixedUpdateOffset)
    {
        newtonPerMeter = Mathf.Max(newtonPerMeter, 0.0001f);

        Vector3 totalForce = Vector3.zero;
        Vector3 totalWeightedPositions = Vector3.zero;

        foreach ((Vector3 force, Vector3 positionFixedUpdate, Color color) in forces)
        {
            if (force.magnitude <= 0f)
                continue;

            Vector3 correctedPosition = positionFixedUpdate + fixedUpdateOffset;

            if (forceSettings.HasFlag(ShowForce.Force))
                Debug.DrawRay(correctedPosition, force / newtonPerMeter, color);

            if (forceSettings.HasFlag(ShowForce.Force3Axis))
            {
                Debug.DrawRay(correctedPosition, new Vector3(force.x, 0f, 0f) / newtonPerMeter, Color.red);
                Debug.DrawRay(correctedPosition, new Vector3(0f, force.y, 0f) / newtonPerMeter, Color.green);
                Debug.DrawRay(correctedPosition, new Vector3(0f, 0f, force.z) / newtonPerMeter, Color.blue);
            }

            if (forceSettings.HasFlag(ShowForce.ForceSum) || forceSettings.HasFlag(ShowForce.Force3AxisSum))
            {
                totalForce += force;
                totalWeightedPositions += (correctedPosition - rb.worldCenterOfMass) * force.magnitude;
            }
        }

        Vector3 forceSumPosition = rb.worldCenterOfMass + totalWeightedPositions / totalForce.magnitude;

        if (forceSettings.HasFlag(ShowForce.ForceSum))
            Debug.DrawRay(forceSumPosition, totalForce / newtonPerMeter, Color.white);

        if (forceSettings.HasFlag(ShowForce.Force3AxisSum))
        {
            Debug.DrawRay(forceSumPosition, new Vector3(totalForce.x, 0f, 0f) / newtonPerMeter, Color.red);
            Debug.DrawRay(forceSumPosition, new Vector3(0f, totalForce.y, 0f) / newtonPerMeter, Color.green);
            Debug.DrawRay(forceSumPosition, new Vector3(0f, 0f, totalForce.z) / newtonPerMeter, Color.blue);
        }


    }

    private void VisualizeTorques(Vector3 fixedUpdateOffset)
    {
        newtonMeterPer2PI = Mathf.Max(newtonMeterPer2PI, 0.0001f);

        Vector3 torqueSum = Vector3.zero;

        foreach ((Vector3 torque, Vector3 positionFixedUpdate, Color color) in torques)
        {
            if (torque.magnitude <= 0f)
                continue;

            Vector3 correctedPosition = positionFixedUpdate + fixedUpdateOffset;

            if (torqueSettings.HasFlag(ShowTorque.Torque))
                DrawArc(correctedPosition, torque, torqueRadius_M, torque.magnitude * 360 / newtonMeterPer2PI, color);

            if (torqueSettings.HasFlag(ShowTorque.Torque3Axis))
            {
                DrawArc(correctedPosition, new Vector3(torque.x, 0f, 0f), torqueRadius_M, torque.x * 360 / newtonMeterPer2PI, Color.red);
                DrawArc(correctedPosition, new Vector3(0f, torque.y, 0f), torqueRadius_M, torque.y * 360 / newtonMeterPer2PI, Color.green);
                DrawArc(correctedPosition, new Vector3(0f, 0f, torque.z), torqueRadius_M, torque.z * 360 / newtonMeterPer2PI, Color.blue);
            }

            if (torqueSettings.HasFlag(ShowTorque.TorqueSum) || torqueSettings.HasFlag(ShowTorque.Torque3AxisSum))
            {
                torqueSum += torque;
            }
        }

        if (torqueSettings.HasFlag(ShowTorque.TorqueSum))
        {
            DrawArc(rb.worldCenterOfMass, torqueSum, torqueRadius_M, torqueSum.magnitude * 360 / newtonMeterPer2PI, Color.white);
        }

        if (torqueSettings.HasFlag(ShowTorque.Torque3AxisSum))
        {
            DrawArc(rb.worldCenterOfMass, new Vector3(torqueSum.x, 0f, 0f), torqueRadius_M, torqueSum.x * 360 / newtonMeterPer2PI, Color.red);
            DrawArc(rb.worldCenterOfMass, new Vector3(0f, torqueSum.y, 0f), torqueRadius_M, torqueSum.y * 360 / newtonMeterPer2PI, Color.green);
            DrawArc(rb.worldCenterOfMass, new Vector3(0f, 0f, torqueSum.z), torqueRadius_M, torqueSum.z * 360 / newtonMeterPer2PI, Color.blue);
        }
    }

    private void DrawArc(Vector3 center, Vector3 normal, float radius, float angle, Color color)
    {
        angle = Mathf.Abs(angle);

        int fullCircles = 0;
        while (angle > 360f && fullCircles < 10)
        {
            DrawArc(center, normal, radius, 360f, color);
            radius += circleSeparation;
            angle -= 360f;
            fullCircles++;
        }

        if (fullCircles >= 10)
        {
            color = Color.magenta;
            angle = angle % 360f;
        }

        Vector3 perpendicular = Vector3.Cross(normal, Vector3.up);

        if (perpendicular.magnitude < 0.001f)
            perpendicular = Vector3.Cross(normal, Vector3.right);

        perpendicular.Normalize();

        Vector3 from = Quaternion.AngleAxis(0 / 2, normal) * perpendicular * radius;

        Debug.DrawRay(center, from, color);

        int segments = 16;
        float step = -angle / segments;
        for (int i = 0; i < segments; i++)
        {
            Vector3 start = Quaternion.AngleAxis(-step * i, normal) * perpendicular * radius;
            Vector3 end = Quaternion.AngleAxis(-step * (i + 1), normal) * perpendicular * radius;

            Debug.DrawLine(center + start, center + end, color);
        }
    }
}
